import Vue from 'vue';
import Router from 'vue-router';
// import PageHome from '@/pages/PageHome';
import PageThreads from '@/pages/operating_systems/PageThreads';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: PageThreads,
    },
    {
      path: '/sistemas-operativos/hilos',
      name: 'Threads',
      component: PageThreads,
    },
  ],
  mode: 'history',
});
