import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const initialState = {
  operations: {
    suma: {
      key: 'suma',
      elapsedTime: 0,
      name: 'Suma',
      symbol: '+',
      timeToExecute: 0,
      icon: 'https://image.flaticon.com/icons/png/128/2/2664.png',
      value: 1,
      relevance: 5,
      status: '',
      progress: 0,
      timer: {},
    },
    resta: {
      key: 'resta',
      elapsedTime: 0,
      name: 'Resta',
      symbol: '-',
      timeToExecute: 0,
      icon: 'http://www.iconninja.com/files/109/463/757/minus-icon.png',
      relevance: 2,
      status: '',
      progress: 0,
      timer: {},
      value: 1,
    },
    multiplicacion: {
      key: 'multiplicacion',
      elapsedTime: 0,
      name: 'Multiplicación',
      symbol: '*',
      timeToExecute: 0,
      icon: 'https://image.flaticon.com/icons/png/128/43/43165.png',
      relevance: 1,
      status: '',
      progress: 0,
      timer: {},
      value: 1,
    },
    division: {
      key: 'division',
      elapsedTime: 0,
      name: 'División',
      symbol: '/',
      timeToExecute: 0,
      icon: 'http://www.iconsalot.com/asset/icons/dinosoftlabs/ios-and-ul/128/101-division-icon.png',
      relevance: .5,
      status: '',
      progress: 0,
      timer: {},
      value: 1,
    },
  },
  objectiveNumber: {
    value: 0,
    current: 0,
    iterations: 0,
  }
};

function Timer(callback, delay) {
  let id;
  let started;
  let remaining = delay;
  let running;

  this.start = () => {
    running = true;
    started = new Date();
    id = setTimeout(callback, remaining);
  };

  this.pause = () => {
    running = false;
    clearTimeout(id);
    remaining -= new Date() - started;
  };

  this.getTimeLeft = () => {
    if (running) {
      this.pause();
      this.start();
    }
    return remaining;
  };

  this.getStateRunning = () => running;

  this.start();
}

export default new Vuex.Store({
  state: {
    operations: initialState.operations,
    objectiveNumber: initialState.objectiveNumber
  },

  getters: {
    testGetter(state) {
      return state;
    },
  },

  actions: {

    calculateObjectiveNumber({state, commit}) {
      const objectiveNumber = (Math.floor((Math.random() * 1000) + 1));

      commit('setObjectiveNumber', objectiveNumber);
    },

    calculateOperatorValues({ state, commit }, level) {
      const operations = { ...state.operations };

      Object.keys(operations).map((key) => {
        operations[key].timeToExecute = Math.floor((Math.random() * 10) + 3);
        operations[key].value =
          operations[key].relevance *
          (Math.floor((Math.random() *
          (level * 100)) + 1));
        operations[key].inactiveTime = operations[key].value * 2;
      });

      commit('setOperations', operations);
    },

    changeOperationStatus({ state, commit }, { key, newStatus }) {
      const operation = state.operations[key];
      operation.status = newStatus;

      commit('setOperation', { operation, key });

      switch (newStatus) {
        case 'is-executing':
          commit('setTimerForOperator', { operation });
          break;
        case '':
          commit('clearOperator', { operation });
          break;
        case 'is-finished':
          commit('clearTimer', { operation });
          commit('setCurrentNumber', { operation })
        default:
          break;
      }
    },

    resetQueue({ state, commit }) {
      Object.keys(state.operations).map(index => {

        const operation = state.operations[ index ].status = '';

        commit('setOperation', { operation, key: index });
      })
    },

    gameFinished({ state, commit }) {
      Object.keys(state.operations).map(index => {

        const operation = state.operations[ index ].status = 'is-paused';

        commit('setOperation', { operation, key: index });
      })
    },

    restartGame({state, commit}) {
      commit('reset', state);
    },

    pauseOperator({state, commit}, { operator, quantum } ) {
      const operation = operator;
      operation.status = 'is-paused';
      operation.elapsedTime = quantum;

      commit('setOperation', { operation, key: operator.key });

    }

  },

  mutations: {

    setObjectiveNumber(state, number) {
      Vue.set(state.objectiveNumber, 'value', number);
    },

    setOperation(state, operation, key) {

      if (key !== undefined) {
        Vue.set(state.operations, key, operation);
      }
    },

    setOperations(state, newOperations) {
      Object.keys(state.operations).map((key) => {
        Vue.set(state.operations, key, newOperations[key]);
      });
    },

    setTimerForOperator(state, { operation }) {
      if (Object.keys(operation.timer).length === 0) {
        const timer = new Timer(() => {
        }, operation.timeToExecute * 1000);
        Vue.set(operation, 'timer', timer);
      }
    },

    clearOperator(state, { operation }) {
      Vue.set(operation, 'timer', {});
      Vue.set(operation, 'progress', 0);
    },

    clearTimer(state, { operation }) {
      Vue.set(operation, 'timer', {});
    },

    setCurrentNumber(state, { operation }) {
      let newValue = 0;

      switch (operation.symbol) {
        case '+':
          newValue = state.objectiveNumber.current + operation.value;
          break;
        case '-':
          newValue = state.objectiveNumber.current - operation.value;
          break;
        case '/':
          newValue = state.objectiveNumber.current / operation.value;
          break;
        case '*':
          newValue = state.objectiveNumber.current * operation.value;
          break;
      }

      if (newValue < 0 )
        newValue = 0;

      Vue.set(state.objectiveNumber, 'current', newValue );
      Vue.set(state.objectiveNumber, 'iterations', state.objectiveNumber.iterations += 1 );
    },

    reset (state) {
      Object.keys(state).map( key => {
        console.log(key);
        Vue.set(state, key, initialState[key]);
      });
    }
  },
});
