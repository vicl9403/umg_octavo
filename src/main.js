// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import '../node_modules/@fortawesome/fontawesome/styles.css';
import FontAwesomeIcon from '../node_modules/@fortawesome/vue-fontawesome';
import '../node_modules/@fortawesome/fontawesome-free-solid';
import VueWorker from '../node_modules/vue-worker';
import store from './store';

Vue.component(FontAwesomeIcon.name, FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.use(VueWorker);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
